'use strict';

const AWS = require('aws-sdk');
const uuid = require('uuid');
const documentClient = new AWS.DynamoDB.DocumentClient(); 


module.exports.create = (event, context, callback) =>{
	console.log(event)
	const data = JSON.parse(event.body);

	const params = {
    TableName : process.env.DYNAMODB_TABLE,
		Item: {
			id: uuid.v1(),
			text: data.text
		},
	};

	documentClient.put(params, (err) =>{
    const response = {
      statusCode: 200,
      body: JSON.stringify(params.Item),
    };
		callback(null, response);
	});
}