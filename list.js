'use strict';

const AWS = require('aws-sdk');
const documentClient = new AWS.DynamoDB.DocumentClient(); 

const params = {
    TableName : process.env.DYNAMODB_TABLE
};
module.exports.list = (event, context, callback) =>{
    documentClient.scan(params, (error, result)=>{
        const response = {
            statusCode: 200,
            body: JSON.stringify(result.Items),
        };
        callback(null, response)
    })
}




